require 'spec_helper'

describe Todo do

  it "valid with name" do
    expect(Todo.new(name: 'something', finished: false)).to be_valid
  end

  it "invalid with no name" do
    expect(Todo.new(name: '')).to be_invalid
  end

end

